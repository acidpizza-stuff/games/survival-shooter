# Survival Shooter

[[_TOC_]]

## Game Info

Try the game at [Gitlab Pages](https://acidpizza-stuff.gitlab.io/games/survivalshooterbuild/) or [Kongregate (Outdated)](https://www.kongregate.com/games/acidpizza/playground)!

<img src="Assets/Icon/screenshot1.png" width="500" height="305"><br>
<img src="Assets/Icon/screenshot2.png" width="500" height="373"><br>
<img src="Assets/Icon/survivalshootericon.jpeg" width="500" height="455">


## Sypnosis

Our hero was happily in bed when he heard strange noises. He crawled out of bed and found that his house was under attack by ZOMBIE RABBITS! See, this is what happens when you’re too lazy to fix your fences… you let things in! So while half asleep, he took his gun out from under the bed and went out to kick some ass!


## Game Mechanics

- Some weapons are more effective against certain enemies.
- Grenade launcher takes into account player movement.
- Enemy mage casts a cold whirlwind + lightning. Slow occurs in whole whirlwind area after 1s . Damage occurs in a smaller area after 2s (when lightning strikes).
- Whirlwind slow stacks twice from 2 different whirlwinds. The degree of blueness of the player is the degree of slow. Slow does not damage.
- The boss fight is a puzzle to be solved :)


## Instructions

### Keyboard + Mouse Controls

- w,s,a,d for movement.
- mouse cursor points to direction of shooting.
- mouse left-click to shoot.
- q,e or scroll wheel to change weapon (if you have it).
- +(=),- to zoom.
- space to pause.

### Touchscreen Controls

- left joystick for movement.
- right joystick to rotate and shoot.
- swipe right or left above right joystick to change weapon.

### Accelerometer Controls

- Tilt phone for movement. Neutral is 45 degrees.
- Tap screen to rotate and shoot.
- Tap right or left of bottom right button to change weapon.


## Upload to Kongregate

- Original resolution: 960x600
- Resolution inclusive of unity container: 960x642


## Release Notes

### v6.1 (22/5/2024)

- Updated Joystick UI for touchscreen controls
- Added accelerometer controls
- Fixed hellephant score counter
- Added lifebar for hellephant in survival mode

### v6.0 (12/5/2024)

- Added touchscreen joystick controls
- Added change weapon sound
- Updated UI to accomodate touchscreen joystick controls
- Updated UI to be responsive
- Added instructions menu
- Allow mobile devices to play in full screen

### v5.2 (6/5/2024)

- Port to Unity (2022 version)
- Rename game name
- Rename start menu buttons for clarity
- Give free AR at start of survival mode
- Increase stats size

### v5.1.x (6/9/2020)

- Port to HTML5 in Unity5 (2019 version) 

### v5.1 (27/7/2015)

- Increased response time for scores to update after killing enemies.
- Reduced stats font size to accomodate for more stats.

### v5 (26/7/2015)

- Shifted camera offset upwards slightly, and increased max zoom out threshold. 
- Changed controls:  +/=,- for zoom, scrollwheel or q,e for change weapon.
- Added campaign mode.
  - Added Hellephant boss fight.
  - Possible to complete campaign mode.
- Added hellephant (non-boss) to survival mode.
- Changed algo for grenade kill stats so it's not possible to get > 100% accuracy.
- Fixed bug where enemies could "overkill" the player at the end.
- Revolver replaces pistol (technically more correct)

### v4.4 (16/7/2015)

- Fixed divide by 0 stats bugs.
- Allow camera zoom out slightly.
- Make cursor visible at game over screen

### v4.3 (16/7/2015)

- Added stats page at game end.

### v4.2 (14/7/2015)

- Tweaked the grenade launcher launch algorithm

### v4.1 (13/7/2015)

- Changed magic to blue.
- Increase medikit heal value.
- Drop assault rifle at start.
- Fixed burst shot bug.

### v4 (12/7/2015)

- Caster-type enemy.
- Unique sounds for all attacks + empty magazine.
- New weapon types: grenade launcher.
- Added medi-kit drop.
- Adjusted weapon effectiveness against specific enemy types.
- Limit camera zoom.
- Survival mode gameplay (gets more difficult with time).
- Ability to pause game.

### v3.3 (6/7/2015)

- Updated the crosshair to be vertical and larger to make it easier to see it when it overlaps with enemies.

### v3.2 (5/7/2015)

- Crosshair replaces mouse cursor for more accurate targeting.
- Start menu implemented.

### v3.1 (2/7/2015)

- Fixed bug where enemy shooters continued shooting after death.

### v3 (1/7/2015)

- Crosshair to player
- New weapon types: pistol, assault rifle, pulse rifle
- Ammo count
- Ability to change weapon 
- Enemies drop powerup when dead
- Shooter-type enemy

### v2 (23/6/2015)

- Changed scenery: added house, fields ground texture, fences, trees.
- Changed controls to mouse-based turning and shooting.

### v1 (22/6/2015)

- Basic scene with some blocks as walls and obstacles. 
- Keyboard-based controls for movement, turning, strafing and shooting.
- Melee-type enemy