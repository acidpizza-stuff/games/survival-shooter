using UnityEngine;
using UnityEngine.InputSystem;

public class SensorReader : MonoBehaviour
{
    public Vector3 acceleration = Vector3.zero;
    //public Vector3 angularVelocity = Vector3.zero;
    //public Vector3 attitude = Vector3.zero;
    //public Vector3 gravity = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
        InputSystem.EnableDevice(Accelerometer.current);
        //InputSystem.EnableDevice(AttitudeSensor.current);
        //InputSystem.EnableDevice(Gyroscope.current);
        //InputSystem.EnableDevice(GravitySensor.current);
    }

    // Update is called once per frame
    void Update()
    {
        acceleration = Accelerometer.current.acceleration.ReadValue();

        // offset accelerometer tilt
        acceleration = Quaternion.Euler(-45, 0, 0) * acceleration;

        //angularVelocity = Gyroscope.current.angularVelocity.ReadValue();
        //attitude = AttitudeSensor.current.attitude.ReadValue().eulerAngles;
        //GravitySensor = GravitySensor.current.gravity.ReadValue();
    }
}
