﻿using UnityEngine;

public class GameConfig : MonoBehaviour
{
	public enum GameMode
	{
		Campaign, Survival
	};

	public enum Difficulty
	{
		Chill, Challenge, Hardcore
	};

    public enum InputMode
    {
        KeyboardMouse, Touchscreen, Accelerometer
    };

    public static GameMode gameMode = GameMode.Campaign;
	public static Difficulty difficulty = Difficulty.Challenge;
	public static float zoom = 30f;
	public static InputMode inputMode = InputMode.Accelerometer;

	void Awake()
	{
		BulletTracker.Clear ();
	}
}
