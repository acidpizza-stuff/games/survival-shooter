using UnityEngine;

public class EnemyHealth_Hellephant : EnemyHealth
{
	public bool bossMode = true;
	EnemyMovement_Hellephant hellephantMovement;
	bool secondForm = false;
	HealthUI_Hellephant healthUI;
	GameOverManager gameOverManager;

	protected override void Awake ()
	{
		base.Awake ();
		hellephantMovement = GetComponent<EnemyMovement_Hellephant> ();
		healthUI = GetComponentInChildren<HealthUI_Hellephant> ();
		healthUI.SetHealth (startingHealth);
	
		gameOverManager = FindObjectOfType(typeof(GameOverManager)) as GameOverManager;
	}


	protected override void Update ()
    {
		base.Update ();
    }

    public override void TakeDamage (int amount)
    {
        if(!isDead)
		{
	        enemyAudio.Play ();

	        currentHealth -= amount;
			healthUI.TakeDamage(amount);

	        if(currentHealth <= 0)
	        {
	            Death ();
	        }
			else if(bossMode && !secondForm && currentHealth <= 4f/10f * startingHealth)
			{
				TakeSecondForm();
			}
		}
    }

	void TakeSecondForm()
	{
		hellephantMovement.TakeSecondForm ();
		gameManager.ActivateBossSecondForm ();
		healthUI.SetLowHealthStatus ();
		secondForm = true;
	}

	public bool IsSecondForm()
	{
		return secondForm;
	}

	protected override void Death()
	{
		if (bossMode)
		{
            hellephantMovement.RevertToFirstForm();
            Invoke("KillAllEnemies", 3f);
            gameManager.WinGame();
        }
        base.Death ();
	}

	public override void StartSinking ()
	{
		GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;
		isSinking = true;
		Destroy (gameObject, 5f); // need to give more time to allow KillAllEnemies() to occur in 3s
	}

	void KillAllEnemies()
	{
        GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject enemy in enemies)
		{
			enemy.GetComponent<EnemyHealth>().TakeDamage(99999);
		}
		Invoke ("Win", 2f);
	}

	void Win()
	{
		gameOverManager.Win ();
	}
}
