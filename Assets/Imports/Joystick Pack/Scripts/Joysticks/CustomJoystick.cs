﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomJoystick : Joystick
{
    private bool hideJoysticks = false;

    Image axisBackground;
    Image handleImage;
    bool joystickActivated = false;
    float fadeTime = 1f;
    RectTransform rectTransform;
    Canvas canvas_;

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (joystickActivated || (!joystickActivated && Math.Abs(eventData.position.y - transform.position.y) < rectTransform.rect.height / 2 * canvas_.scaleFactor))
        {
            // show joysticks
            joystickActivated = true;
            axisBackground.color = new Color(1, 1, 1, 0.2f);
            handleImage.color = new Color(1, 1, 1, 0.6f);

            base.OnPointerDown(eventData);
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {

        base.OnPointerUp(eventData);

        if (hideJoysticks)
        {
            // hide joysticks
            joystickActivated = false;
            StartCoroutine(FadeTo(axisBackground, 0, fadeTime));
            StartCoroutine(FadeTo(handleImage, 0, fadeTime));
        }
    }

    private void Awake()
    {
        rectTransform = gameObject.GetComponent<RectTransform>();
        canvas_ = GetComponentInParent<Canvas>();

        axisBackground = gameObject.GetComponent<Image>();       
        foreach (Transform tr in transform)
        {
            if ( tr.name == "Handle")
            {
                handleImage = tr.gameObject.GetComponent<Image>();
            }
        }
        if (hideJoysticks)
        {
            // hide joysticks
            joystickActivated = false;
            StartCoroutine(FadeTo(axisBackground, 0, fadeTime));
            StartCoroutine(FadeTo(handleImage, 0, fadeTime));
        }
    }

    IEnumerator FadeTo(Image image, float desiredAlphaValue, float fadeTime)
    {
        float alpha = image.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadeTime)
        {
            if (joystickActivated)
            {
                yield break;
            }
            image.color = new Color(1, 1, 1, Mathf.Lerp(alpha, desiredAlphaValue, t));
            yield return null;
        }
    }
}