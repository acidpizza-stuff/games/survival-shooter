﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    Animator anim;
	public Text gameOverText;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

	public void GameOver()
	{
		anim.SetTrigger("GameOver");
		Cursor.visible = true;
	}

	public void RestartApplication()
	{
		SceneManager.LoadScene("StartMenu");
    }

	public void Win()
	{
		gameOverText.text = "YOU WIN!!!";
		GameOver ();
	}
}
