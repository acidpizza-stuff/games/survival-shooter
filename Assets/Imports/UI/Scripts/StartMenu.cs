using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StartMenu : MonoBehaviour 
{
	public Toggle chillToggle, challengeToggle, hardcoreToggle;
	public Toggle keyboardMouseToggle, touchscreenToggle;

	void Start()
	{ 
		Cursor.visible = true;
	}

	void Awake()
	{
		if(GameConfig.difficulty == GameConfig.Difficulty.Chill)          { chillToggle.isOn = true; }
		else if(GameConfig.difficulty == GameConfig.Difficulty.Challenge) {	challengeToggle.isOn = true; }
		else if(GameConfig.difficulty == GameConfig.Difficulty.Hardcore)  { hardcoreToggle.isOn = true; }

		if (GameConfig.inputMode == GameConfig.InputMode.KeyboardMouse)    { keyboardMouseToggle.isOn = true; }
		else if (GameConfig.inputMode == GameConfig.InputMode.Touchscreen) { touchscreenToggle.isOn = true; }
    }

	public void OnCampaign()
	{
		GameConfig.gameMode = GameConfig.GameMode.Campaign;
        SceneManager.LoadScene("Level1");
    }

    public void OnSurvival()
	{
		GameConfig.gameMode = GameConfig.GameMode.Survival;
        SceneManager.LoadScene("Level1");
    }

	public void OnDifficultyChill(bool active)
	{
		if(active) {GameConfig.difficulty = GameConfig.Difficulty.Chill; }
	}

	public void OnDifficultyChallenge(bool active)
	{
		if(active) {GameConfig.difficulty = GameConfig.Difficulty.Challenge; }
	}

	public void OnDifficultyHardcore(bool active)
	{
		if(active) {GameConfig.difficulty = GameConfig.Difficulty.Hardcore; }
	}

	public void OnInputKeyboardMouse(bool active)
	{
		if(active) { GameConfig.inputMode = GameConfig.InputMode.KeyboardMouse; }
	}

    public void OnInputTouchscreen(bool active)
    {
        if (active) { GameConfig.inputMode = GameConfig.InputMode.Touchscreen; }
    }

    public void OnInputAccelerometer(bool active)
    {
        if (active) { GameConfig.inputMode = GameConfig.InputMode.Accelerometer; }
    }
}
